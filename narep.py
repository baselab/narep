#!/usr/bin/env python
# -*- coding: utf-8 -*-

version = "0.1a"

import sys
import datetime
if int(sys.version[0]) < 3:
    from ConfigParser import SafeConfigParser
else:
    from configparser import SafeConfigParser

sys.path.append("lib")
from GetData import *
from ProcData import *


def print_help():
    "Print out usage and help."
    print("""
    {0} v{1}

    Usage: {2} [ <option> | <group> ]

    <option> in one of the following:
    {3}  Print configuration
    {4}  Show this help
    {5}  Print version

    <group> is one of the following:

    """.format(prog_desc, version, prog, conf_opt[1], help_opt[1], vers_opt[1]) \
    + ", ".join(groups) \
    + "\n\n    Check config to find members of each one.\n")

def print_conf():
    "Print out saved configuration."
    print
    for section in parser.sections():
        print("Section '{0}'".format(section))
        for opt, value in parser.items(section):
            print("  {0}: {1}".format(opt, value))
        print

def ck_args():
    "Check arguments correctness and related actions."
    if (args != 2) or (arg1 in help_opt):
        print_help()
        sys.exit(1)
    elif (arg1 in conf_opt):
        print_conf()
        sys.exit(0)
    elif (arg1 in vers_opt):
        print("{0} {1}".format(prog_name, version))
        sys.exit(0)
    elif (arg1 not in groups):
        print("Error:\n\n {0} : not a valid group. Check config.\n".format(arg1))
        sys.exit(1)

def get_hosts():
    "Fetch list of hosts based on given group."
    hosts = parser.get("hosts", arg1).split(',')
    return hosts

def get_apilist():
    "Fetch list of API from config."
    apilist = parser.get("API", "apilist").split(',')
    return apilist


def main():
    "Start the program."

    ck_args()

    for filer in get_hosts():
        rawdata = GetData(filer, servertype, loginstyle, trasport, port, majapi, minapi, user, password)
        apilist = get_apilist()
        pdata = ProcData(filer, date, apilist, dir_xml, sqlitedb)
        pdata.match_save(rawdata)

        xfiles = pdata.listData()
        pdata.xmlToSQL(xfiles)


args = len(sys.argv)
prog = sys.argv[0]
if (args > 1):
    arg1 = sys.argv[1]

prog_name = "narep"
prog_desc = "NetApp Reports"

help_opt = [ 'h', '-h', '-H', '--help', '-help', 'help', '?' ]
conf_opt = [ 'c', '-c' ]
vers_opt = [ 'v', '-v', '-V', '--version', '-version', 'version' ]

config = "config.ini"
parser = SafeConfigParser()
parser.read(config)

servertype = parser.get("connect", "servertype")
loginstyle = parser.get("connect", "loginstyle")
trasport = parser.get("connect", "trasport")
port = parser.get("connect", "port")
majapi = parser.get("API", "majapi")
minapi =parser.get("API", "minapi")
user = parser.get("credentials", "user")
password = parser.get("credentials", "password")

groups = parser.options("hosts")

dir_base = parser.get("paths", "datadir_base")
dir_xml = dir_base + "/xml/"
sqlitedb = dir_base + "/" + parser.get("paths", "sqlitedb")
today = datetime.date.today()
date = today.strftime('%Y%m%d')


if __name__ == "__main__":
    main()
