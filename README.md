# NetApp Filers Report Tool

Please read LICENSE.txt

## Description
NetApp filers expose a nice (?) API which can be used to get various informations
about almost everything or to send commands to create, destroy, modify filers
characteristics/behaviour/..

`narep' is an exercise intended to learn some of the NetApp API but it stops
at producing some reports (actually not yet).

Program workflow is something like:

* Create lists of hosts and APIs based on args and config
* Retrieve raw data and save it to XML files
* Fill a sqlite DB with parsed XML data

## Usage

### Depen
It should run fine either with Python 2.7.x or 3.4.x

To try it out you'll need

* NaElement.py
* NaServer.py

from the NetApp sdk. Put them in the lib/ directory.

### Config
There is a config file where I've put some defaults.

This config presumes all filer have the same user/passwd (a prompt that asks
credentials can be easily done).

Files to create:

* ./$datadir_base [dir]
* ./$datadir_base/xml [dir]
* ./$datadir_base/$sqlitedb [SQlite DB]

### Running
From a decent terminal:

`python narep.py -h`

