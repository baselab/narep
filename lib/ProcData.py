#!/usr/bin/env python

import os
import re
from lxml import etree
import sqlite3

class ProcData :
    "Transform XML to something more readable."

    def __init__(self, filer, date, apilist, dir_xml, sqlitedb):
        self.filer = filer
        self.date = date
        self.apilist = apilist
        self.dir_xml = dir_xml
        self.sqlitedb = sqlitedb

    def save_xml(self, desc, content):
        "Write data to XML file."
        xmlfile = self.dir_xml + self.date + "_" + self.filer + "@" + desc + ".xml"
        fo = open(xmlfile, "wb")
        fo.write(content.encode())
        fo.close()

    def match_save(self, rawdata):
        "Do match API<->desc and retrieve data -> save."
        apilist = self.apilist

        if ("system-get-version" in apilist):
            desc = "version"
            #content = rawdata.sysVersion() #ORIG
            content = """<results status="passed">
        <version>NetApp Release 8.1.2P1 7-Mode: Mon Jan  7 15:55:05 PST 2013</version>
        <is-clustered>false</is-clustered>
    </results>\n""" #TBR
            self.save_xml(desc, content)

        if ("system-get-info" in apilist):
            desc = "sysinfo"
            content = rawdata.sysInfo()
            save_xml(filer, desc, content)

        if ("system-get-ontapi-version" in apilist):
            desc = "ontapiver"
            content = rawdata.sysVersapi()
            save_xml(filer, desc, content)

        if ("cf-status" in apilist):
            desc = "cfstat"
            content = rawdata.cfStatus()
            save_xml(filer, desc, content)

        if ("disk-list-info" in apilist):
            desc = "diskinfo"
            content = rawdata.diskInfo()
            save_xml(filer, desc, content)

        if ("disk-sanown-list-info" in apilist):
            desc = "disksaninfo"
            content = rawdata.diskSanInfo()
            save_xml(filer, desc, content)

        if ("aggr-list-info" in apilist):
            desc = "aggrinfo"
            content = rawdata.aggrInfo()
            save_xml(filer, desc, content)

        if ("vfiler-list-info" in apilist):
            desc = "vfilerinfo"
            content = rawdata.vfilerInfo()
            save_xml(filer, desc, content)

        if ("lun-list-info" in apilist):
            desc = "luninfo"
            content = rawdata.lunInfo()
            save_xml(filer, desc, content)

        if ("nfs-status" in apilist):
            desc = "nfsstat"
            content = rawdata.nfsStatus()
            save_xml(filer, desc, content)

        if ("cifs-status" in apilist):
            desc = "cifsstat"
            content = rawdata.cifsStatus()
            save_xml(filer, desc, content)

    def listData(self):
        "Check gathered data."
        xfiles = []
        exp = re.compile('{0}.*{1}.*'.format(self.date, self.filer))
        for f in os.listdir(self.dir_xml):
                if (exp.match(f)):
                    item = self.dir_xml + f
                    xfiles.append(item)
        return xfiles

    def xmlToSQL(self, xfiles):
        # files for (filer + today)
        for xml in xfiles:

            exp_l = re.compile('^.*@')
            exp_r = re.compile('\.xml$')
            interm = re.sub(exp_l, '', xml)
            desc = re.sub(exp_r, '', interm)

            tree = etree.parse(xml)

            #TODO : check if tables are present and with all filers
            #con = sqlite3.connect(self.sqlitedb)

            # update "version" table
            if desc == "version":
                table = desc
                value = tree.findtext(table)
                dcol = "d" + self.date
                con = sqlite3.connect(self.sqlitedb)
                with con:
                    cur = con.cursor()
                    action = "INSERT INTO " + table + " (filer, " + dcol + ") VALUES('" + self.filer + "', '" + value + "')"
                    print(action) #TBR
                    cur.execute(action)








#
